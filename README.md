
# Teragence Android SDK Integration Documentation for App Developers

## Minimum versions

- This SDK has been tested in apps using AGP version 7.5.0 (Gradle 7.4) to 8.4.0 and Kotlin 1.8.22 to 2.0.0
- minSdk is 21
- Target and source JDK compilation version 17

## Step 1: Add Required Permissions and Activities

Before integrating the SDK, add the necessary permissions to your app's `AndroidManifest.xml`:

```xml
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION"/>
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.READ_PHONE_STATE" /> <!-- Optional -->
```

**Note**: The `READ_PHONE_STATE` permission is optional, only request this if your app requires it.

Also add the `teragencePartnerId` key and value so we can identify your measurements:

```xml
<application>
    ...
    <meta-data
        android:name="teragencePartnerId"
        android:value="<your partner name>" />
    ...
</application>
```

## Step 2: Configure Your Project to Use the SDK

### Add the Repository

Specify the Maven repository in your `settings.gradle.kts` (Kotlin) or `settings.gradle` (Groovy) to include the SDK in your project:

**Kotlin:**

```kotlin
    dependencyResolutionManagement {
        repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
        repositories {
            maven("https://gitlab.com/api/v4/projects/21880832/packages/maven")
            ...
        }
    }
```

**Groovy:**

```groovy
    dependencyResolutionManagement {
        repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
        repositories {
            maven { url 'https://gitlab.com/api/v4/projects/21880832/packages/maven' }
            ...
        }
    }
```

### Add the SDK as a Dependency

In your module’s `build.gradle` or `build.gradle.kts`, add the following line under dependencies:

**Kotlin:**

```kotlin
    dependencies {
        ...
        implementation("com.teragence:client:x.x.x.x")
        ...
    }
```

 **Groovy:**

```groovy
    dependencies {
        ...
        implementation 'com.teragence:client:x.x.x.x'
        ...
    }
```

Where "x.x.x.x" is the latest version in the [release notes](https://gitlab.com/teragence/mobile/android-sdk/-/blob/master/release_notes.md)

### Update proguard rules (optional)

R8 minify can be aggressive when creating a release version - you may need to add these dontwarns to proguard

```text
-dontwarn org.bouncycastle.jsse.BCSSLParameters
-dontwarn org.bouncycastle.jsse.BCSSLSocket
-dontwarn org.bouncycastle.jsse.provider.BouncyCastleJsseProvider
-dontwarn org.conscrypt.Conscrypt$Version
-dontwarn org.conscrypt.Conscrypt
-dontwarn org.conscrypt.ConscryptHostnameVerifier
-dontwarn org.openjsse.javax.net.ssl.SSLParameters
-dontwarn org.openjsse.javax.net.ssl.SSLSocket
-dontwarn org.openjsse.net.ssl.OpenJSSE
```

## Step 3: Import and Use the SDK in Your Application

### Import SDK Components

Include the necessary SDK imports in your Activity or Fragment.

**Kotlin:**

```kotlin
import com.teragence.client.TgSdkMeasurementManager
import com.teragence.client.TgSdkLocationTrigger
```

**Java:**

```java
import com.teragence.client.TgSdkMeasurementManager;
import com.teragence.client.TgSdkLocationTrigger;
```

### Initialise and Use TgSdkMeasurementManager

Instantiate the `TgSdkMeasurementManager` in your Activity or Fragment and use it to perform location measurements, e.g:

**Kotlin:**

```kotlin
class YourActivity : AppCompatActivity() {
    private lateinit var measurementManager: TgSdkMeasurementManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.your_layout)

        measurementManager = TgSdkMeasurementManager(this)
        triggerMeasurement()
    }

    private fun triggerMeasurement() {
        measurementManager.executeMeasurement(
            TgSdkLocationTrigger.CURRENT_LOCATION.ordinal,
            successCallback = { data ->
                Log.d("Measurement", "Success: $data")
            },
            errorCallback = { exception ->
                Log.e("Measurement", "Error: ${exception.localizedMessage}")
            }
        )
    }
}
```

**Java:**

```java
public class YourActivity extends AppCompatActivity {
    private TgSdkMeasurementManager measurementManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.your_layout);

        measurementManager = new TgSdkMeasurementManager(this);
        triggerMeasurement();
    }

    private void triggerMeasurement() {
        measurementManager.executeMeasurement(
            TgSdkLocationTrigger.CURRENT_LOCATION.ordinal(),
            data -> {
                Log.d("Measurement", "Success: " + data);
                return Unit.INSTANCE;
            },
            exception -> {
                Log.e("Measurement", "Error: " + exception.getLocalizedMessage());
                return Unit.INSTANCE;
            }
        );
    }
}
```

## Usage

### LAST_KNOWN_LOCATION

Use this where you are adding the Teragence measurement to an existing location-capturing Activity you have in your app. This is typically faster and uses less battery because it does not require the device to activate its location hardware.

```kotlin
measurementManager.executeMeasurement(
    TgSdkLocationTrigger.LAST_KNOWN_LOCATION.ordinal,
    ...
)
```

### CURRENT_LOCATION

Use this if your app doesn't natively capture location information and you want Teragence to take a current reading. This mode activates the device’s location hardware and may take longer to obtain a location fix. It uses more battery and from Android 11+, it will show a location marker in the status bar as per Android's location access policies.

_Ensure that your application complies with Google’s policies regarding location data. This includes obtaining necessary permissions from users before attempting to access location services and clearly informing users when their location is being accessed, especially in real-time scenarios._

```kotlin
measurementManager.executeMeasurement(
    TgSdkLocationTrigger.CURRENT_LOCATION.ordinal,
    ...
)
```

## Data connection

The SDK will take valid readings while on WiFi, and POST them to our servers without using mobile data. If WiFi is not the primary connection it will send over the mobile data connection, with a payload of under 3KB.

## Logs

Filter by "TgSdkMeasurementManager" for SDK specific messages.

## Issues

Log any bugs or issues in the [gitlab issue tracker](https://gitlab.com/teragence/mobile/android-sdk/-/issues)

---
